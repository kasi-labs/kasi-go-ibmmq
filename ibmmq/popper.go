package ibmmq

import (
	"github.com/ibm-messaging/mq-golang/ibmmq"
	"gitlab.com/kasi-labs/kasi-go-ibmmq/ibmmq/mqutils"
	"gitlab.com/kasi-labs/kasi-go-queue/message"
	"gitlab.com/kasi-labs/kasi-go-queue/queue"
)

type Popper struct {
	*connectionWrapper
}

func NewPopper(config Config) (*Popper, error) {
	conn, err := newConnection(config)
	if err != nil {
		return nil, err
	}

	qObject, err := mqutils.OpenQueue(conn.qMgr, mqutils.Get)
	if err != nil {
		//logger.Fatalf("Unable to Open Queue")
		logError(err)
		return nil, err
	}
	conn.qObject = qObject

	popper := &Popper{conn}
	return popper, nil
}

// Pop value from IBM Queue
func (q *Popper) Pop() (*message.Message, error) {
	q.popWg.Add(1)
	defer q.popWg.Done()

	msg, err := q.getMessage()
	if err != nil {
		return &message.Message{}, err
	}

	return msg, nil
}

func (q *Popper) getMessage() (*message.Message, error) {
	var err error
	var datalen int
	var mqret *ibmmq.MQReturn

	// The PUT requires control structures, the Message Descriptor (MQMD)
	// and Put Options (MQPMO). Create those with default values.
	getmqmd := ibmmq.NewMQMD()

	// Create a buffer for the message data. This one is large enough
	// for the messages put by the amqsput sample.
	buffer := make([]byte, 1024)

	// Now try to get the message
	datalen, err = q.qObject.Get(getmqmd, q.config.gmo, buffer)
	if err != nil {
		mqret = err.(*ibmmq.MQReturn)
		logger.Printf("return code %d, expected %d,", mqret.MQRC, ibmmq.MQRC_NO_MSG_AVAILABLE)
		if mqret.MQRC == ibmmq.MQRC_NO_MSG_AVAILABLE {
			// If there's no message available, then don't treat that as a real error as
			// it's an expected situation
			err = nil
			return nil, queue.ErrQueueIsEmpty
		}
		return nil, err
	}

	msg := message.NewMessage(string(getmqmd.MsgId), buffer[:datalen])
	return msg, err
}
