package ibmmq

import "gitlab.com/kasi-labs/kasi-go-queue/queue"

var NewIbmmqFactory = func(config Config) *queue.NewFactory {
	return &queue.NewFactory{
		NewPopper: func() (queue.Popper, error) { return NewPopper(config) },
		NewPusher: func() (queue.Pusher, error) { return NewPusher(config) },
	}
}
