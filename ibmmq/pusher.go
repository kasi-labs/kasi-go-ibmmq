package ibmmq

import (
	"encoding/hex"
	"github.com/ibm-messaging/mq-golang/ibmmq"
	"gitlab.com/kasi-labs/kasi-go-ibmmq/ibmmq/mqutils"
	"gitlab.com/kasi-labs/kasi-go-queue/message"
	"strings"
)

type Pusher struct {
	*connectionWrapper
}

func NewPusher(config Config) (*Pusher, error) {
	conn, err := newConnection(config)
	if err != nil {
		return nil, err
	}

	qObject, err := mqutils.OpenQueue(conn.qMgr, mqutils.Put)
	if err != nil {
		logError(err)
		return nil, err
	}
	conn.qObject = qObject

	// The Put Options (MQPMO). Create those with default values.
	return &Pusher{conn}, nil
}

// Push value to IBM Queue
func (q *Pusher) Push(messages ...*message.Message) error {
	q.pushWg.Add(1)
	defer q.pushWg.Done()

	for _, msg := range messages {
		if err := q.putMessage(msg, q.config.pmo); err != nil {
			return err
		}
	}
	return nil
}

func (q *Pusher) putMessage(message *message.Message, pmo *ibmmq.MQPMO) error {
	putmqmd, data, err := q.config.Marshaler.Marshal(message)
	if err != nil {
		logError(err)
		return err
	}

	logger.Printf("Sending message %s", data)
	err = q.qObject.Put(putmqmd, pmo, data)
	if err != nil {
		logError(err)
		return err
	} else {
		logger.Println("Put message to", strings.TrimSpace(q.qObject.Name))
		// Print the MsgId so it can be used as a parameter to amqsget
		logger.Println("MsgId:" + hex.EncodeToString(putmqmd.MsgId))
	}

	return nil
}
