package ibmmq

import (
	"github.com/ibm-messaging/mq-golang/ibmmq"
	"gitlab.com/kasi-labs/kasi-go-queue/message"
)

type Marshaler interface {
	Marshal(msg *message.Message) (*ibmmq.MQMD, []byte, error)
}

type DefaultMarshaler struct {
}

func (d DefaultMarshaler) Marshal(msg *message.Message) (*ibmmq.MQMD, []byte, error) {
	// The PUT requires control structures, the Message Descriptor (MQMD)
	putmqmd := ibmmq.NewMQMD()

	// Tell MQ what the message body format is. In this case, a text string
	putmqmd.Format = ibmmq.MQFMT_STRING

	return putmqmd, msg.Payload, nil
}
