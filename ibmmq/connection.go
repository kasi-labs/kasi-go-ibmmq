package ibmmq

import (
	"github.com/ibm-messaging/mq-golang/ibmmq"
	"gitlab.com/kasi-labs/kasi-go-ibmmq/ibmmq/mqutils"
	"io/ioutil"
	"log"
	"sync"
)

var logger = log.New(ioutil.Discard, "Env: ", log.LstdFlags)

type connectionWrapper struct {
	config  Config
	qMgr    ibmmq.MQQueueManager
	qObject ibmmq.MQObject

	pushWg sync.WaitGroup
	popWg  sync.WaitGroup
}

func newConnection(config Config) (*connectionWrapper, error) {
	conn := &connectionWrapper{
		config: config,
	}

	if err := conn.connect(); err != nil {
		return &connectionWrapper{}, err
	}

	return conn, nil
}

// Establishes the connection to the MQ Server. Returns the
// Queue Manager if successful
func (c *connectionWrapper) connect() error {
	qMgr, err := mqutils.CreateConnection(c.config.Connection)
	if err != nil {
		logError(err)
		return err
	}
	logger.Println("Connected to IBM MQ")
	c.qMgr = qMgr

	return nil
}

func (c *connectionWrapper) Close() error {
	if err := c.qObject.Close(0); err != nil {
		return err
	}
	return nil
}

func logError(err error) {
	logger.Println(err)
	logger.Printf("Error Code %v", err.(*ibmmq.MQReturn).MQCC)
}
