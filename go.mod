module gitlab.com/kasi-labs/kasi-go-ibmmq

go 1.12

require (
	github.com/ibm-messaging/mq-golang v0.0.0-20190820103725-19b946c185a8
	gitlab.com/kasi-labs/kasi-go-queue v0.1.0
)
